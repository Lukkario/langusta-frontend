import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { AuthService } from '../auth.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css', '../bootstrap.css']
})
export class SettingsComponent implements OnInit {

  form:FormGroup;

  constructor(private http: HttpClient, private fb:FormBuilder, private Auth: AuthService) {
    this.form = this.fb.group({
      currentPassword: ['', {validators: [Validators.required], updateOn: 'blur'}],
      newPassword: ['', {validators: [Validators.required], updateOn: 'blur'}],
      confirmPassword: ['', {validators: [Validators.required], updateOn: 'blur'}]
    });
  }

  ngOnInit() {
    this.getUserData();
  }

  meEmail = "";
  meFirstName = "";
  meLastName = "";
  token = sessionStorage.getItem('token');

  fbName = "";
  fbPicUrl = "";
  fbError = "";

  fbTokens = [];
  fbTokenError = "";

  getUserData()
  {
    this.http.get<any>('/api/user/me').subscribe(
      res =>
      {
        console.log(res);
        this.meEmail = res.payload.email;
        this.meFirstName = res.payload.firstName;
        this.meLastName = res.payload.lastName;
      }
    );
  }

  changePassword(): void
  {
    document.getElementById("error").style.display = "none";
    document.getElementById("success").style.display = "none";
    const val = this.form.value;
    if(val.newPassword !== val.confirmPassword)
    {
      console.log("1")
      document.getElementById("error").innerHTML = "<strong>Incorrect passwords!</strong> Passwords do not match up.</strong>"
      document.getElementById("error").style.display = "block";
    }
    else
    {
      if(val.newPassword && val.confirmPassword && val.currentPassword)
      {
        const currentPassword = val.currentPassword;
        const newPassword = val.newPassword;
        // console.log(currentPassword + " " + newPassword)
        this.http.put<any>('/api/user', {currentPassword, newPassword}).subscribe(
          res => {
            console.log(res)
              if(res.state === "STATUS_SUCCESS")
              {
                document.getElementById("success").style.display = "block";
              }
          },
          err =>
          {
            console.log(err)
            document.getElementById("error").innerHTML = "<strong>"+ err.error.additionalInfo +"</strong>"
            document.getElementById("error").style.display = "block";
          }
        )
      }
    }
  }

  get currentPassword()
  {
    return this.form.get('currentPassword');
  }

  get newPassword()
  {
    return this.form.get('newPassword');
  }

  get confirmPassword()
  {
    return this.form.get('confirmPassword');
  }
}
