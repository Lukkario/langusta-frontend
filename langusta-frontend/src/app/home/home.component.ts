import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, ComponentRef, ComponentFactory} from '@angular/core';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { LandpadComponent } from '../landpad/landpad.component';
import { AddFbPostComponent } from '../add-fb-post/add-fb-post.component';
import { SendMessageFbComponent } from '../send-message-fb/send-message-fb.component';
import { ContestsComponent } from '../contests/contests.component';
import { SettingsComponent } from '../settings/settings.component';
import { AuthService } from '../auth.service';
import { Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css', '../bootstrap.css']
})

export class HomeComponent implements OnInit {

  @ViewChild('dashboardcontainer', {read: ViewContainerRef}) dash: ViewContainerRef;
  @ViewChild('landpadcontainer', {read: ViewContainerRef}) landpad: ViewContainerRef;
  @ViewChild('addfbpostcontainer', {read: ViewContainerRef}) addfbpost: ViewContainerRef;
  @ViewChild('sendmessagefbcontainer', {read: ViewContainerRef}) sendmessagefb: ViewContainerRef;
  @ViewChild('makecontestcontainer', {read: ViewContainerRef}) makecontest: ViewContainerRef;
  @ViewChild('settingscontainer', {read: ViewContainerRef}) settings: ViewContainerRef;
  componentRef: any;

  constructor(private resolver: ComponentFactoryResolver, private Auth: AuthService, private router: Router) {
    if(this.Auth.isLoggedOut())
    {
      this.router.navigateByUrl("/login");
    }
  }

  ngOnInit() {
    if(!this.isAnyComponentLoaded())
    {
      this.viewLandpadComponent();
    }
  }

  public viewDashboardComponent(): void
  {
    this.destroyComponent();
    this.dash.clear();
    const factory = this.resolver.resolveComponentFactory(DashboardComponent);
    this.componentRef = this.dash.createComponent(factory);
  }

  public viewLandpadComponent(): void
  {
    this.destroyComponent();
    this.landpad.clear();
    this.componentRef = this.landpad.createComponent(this.resolver.resolveComponentFactory(LandpadComponent));
  }

  public viewAddFbPostComponent(): void
  {
    this.destroyComponent();
    this.addfbpost.clear();
    this.componentRef = this.addfbpost.createComponent(this.resolver.resolveComponentFactory(AddFbPostComponent));
  }

  public viewSendMessageFb(): void
  {
    this.destroyComponent();
    this.sendmessagefb.clear();
    this.componentRef = this.sendmessagefb.createComponent(this.resolver.resolveComponentFactory(SendMessageFbComponent));
  }

  public viewMakeContest(): void
  {
    this.destroyComponent();
    this.makecontest.clear();
    this.componentRef = this.makecontest.createComponent(this.resolver.resolveComponentFactory(ContestsComponent))
  }

  public viewSettings(): void
  {
    this.destroyComponent();
    this.settings.clear();
    this.componentRef = this.settings.createComponent(this.resolver.resolveComponentFactory(SettingsComponent))
  }

  private destroyComponent(): void
  {
    if(this.componentRef !== undefined)
    {
      this.componentRef.destroy();
    }
  }

  private isAnyComponentLoaded(): boolean
  {
    if(this.componentRef !== undefined)
    {
      return true;
    }
    return false;
  }

}
