import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css', '../bootstrap.css']
})
export class DashboardComponent implements OnInit {

  constructor(private http: HttpClient) {
    this.getAllContests();
  }

  ngOnInit() {
  }
  meEmail = "";
  meFirstName = "";
  meLastName = "";
  uid = sessionStorage.getItem('uid');

  fbName = "";
  fbPicUrl = "";
  fbError = "";

  fbTokens = [];
  fbTokenError = "";

  contests = [];

  getAllContests()
  {
    this.http.get<any>('/api/contest/all').subscribe(
      res =>
      {
        console.log(res)
        this.contests = res.payload;
      },
      err =>
      {
        console.log(err)
      }
    )
  }

  test()
  {
    this.http.get<any>('/api/user/me').subscribe(
      res =>
      {
        console.log(res);
        this.meEmail = res.payload.email;
        this.meFirstName = res.payload.firstName;
        this.meLastName = res.payload.lastName;
      }
    );
  }

  test2()
  {
    this.http.get<any>('/api/facebook/tokens').subscribe(
      res =>
      {
        document.getElementById("fbTokenError").style.display = "none";
        console.log(res);
        if(res.state === "STATUS_SUCCESS")
        {
          this.fbTokens = res.payload;
          console.log(this.fbTokens);
        }
        else if(res.state === "STATUS_ERROR")
        {
          this.fbTokens = [];
          document.getElementById("fbTokenErrorMessage").innerHTML = res.payload.message;
          document.getElementById("fbTokenError").style.display = "block";
        }
      }
    );
  }

  test3()
  {
    document.getElementById("fbData").style.display = "none";
    document.getElementById("fbDataError").style.display = "none";
    this.http.get<any>('/api/facebook/profile/me').subscribe(
      res =>
      {
        console.log(res)
        if(res.state === "STATUS_SUCCESS")
        {
          this.fbName = res.payload.name;
          this.fbPicUrl = res.payload.picture.data.url;
          document.getElementById("fbData").style.display = "block";
        }
        else if(res.state === "STATUS_ERROR")
        {
          document.getElementById("fbDataErrorMessage").innerHTML = res.payload.message;
          document.getElementById("fbDataError").style.display = "block";
        }
      }
    );
  }
}
