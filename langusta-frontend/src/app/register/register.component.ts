import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
declare var grecaptcha: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css', '../bootstrap.css']
})
export class RegisterComponent implements OnInit {

  form:FormGroup;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";

  constructor(private fb:FormBuilder, private Auth: AuthService)
  {
    this.form = this.fb.group({
      firstName: ['',{validators: [Validators.required], updateOn: 'blur'}],
      lastName: ['',{validators: [Validators.required], updateOn: 'blur'}],
      email: ['',{validators: [Validators.required, Validators.pattern(this.emailPattern)], updateOn: 'blur'}],
      password: ['', {validators: [Validators.required], updateOn: 'blur'}],
      passwordConfirm: ['', {validators: [Validators.required], updateOn: 'blur'}]
    });
  }

  ngOnInit() {
  }

  register(): void
  {
    document.getElementById("error").style.display = "none";
    document.getElementById("success").style.display = "none";
    const val = this.form.value;
    if(val.password !== val.passwordConfirm)
    {
      document.getElementById("error").innerHTML = "<strong>Incorrect password!</strong> Passwords do not match up."
      document.getElementById("error").style.display = "block";
    }
    const response = grecaptcha.getResponse()
    if(response.length === 0)
    {
      document.getElementById("error").innerHTML = "<strong>Captcha not filled!</strong> Please fill in captcha."
      document.getElementById("error").style.display = "block";
    }
    else
    {
      if(val.email && val.password && val.firstName && val.lastName)
      {
        this.Auth.registerUser(val.firstName, val.lastName, val.email, val.password).subscribe(
          res => {
            if(res.state === "STATUS_SUCCESS")
            {
              document.getElementById("success").style.display = "block";
            }
            else
            {
               document.getElementById("error").innerHTML = "<strong>User exists!</strong> User with that email address already exists.";
               document.getElementById("error").style.display = "block";
               grecaptcha.reset()
            }
          },
          err =>
          {
            if(err.error.state === "STATUS_ERROR" && err.error.additionalInfo === "User already exists.")
            {
              document.getElementById("error").innerHTML = "<strong>User exists!</strong> User with that email address already exists.";
              document.getElementById("error").style.display = "block";
              grecaptcha.reset()
            }
          }
        )
      }
    }
  }

  get email()
  {
    return this.form.get('email');
  }

  get firstName()
  {
    return this.form.get('firstName');
  }

  get lastName()
  {
    return this.form.get('lastName');
  }

  get password()
  {
    return this.form.get('password');
  }

  get passwordConfirm()
  {
    return this.form.get('passwordConfirm');
  }

}
