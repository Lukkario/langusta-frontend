import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css', '../bootstrap.css', '../simple-line-icons.css', './device-mockups.css', './new-age.css']
})
export class WelcomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  ToggleResMenu()
  {
    const menuStyle = document.getElementById("navbarResponsive")
    if(menuStyle.style.display === "block")
    {
      menuStyle.style.display = "none";
    }
    else
    {
      menuStyle.style.display = "block";
    }
  }

}
