import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandpadComponent } from './landpad.component';

describe('LandpadComponent', () => {
  let component: LandpadComponent;
  let fixture: ComponentFixture<LandpadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandpadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandpadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
