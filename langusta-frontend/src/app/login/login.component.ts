import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { RouterModule, Routes, Router } from '@angular/router';
import * as jwt_decode from "jwt-decode";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css', '../bootstrap.css']
})
export class LoginComponent implements OnInit {

  form:FormGroup;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";

  constructor(private fb:FormBuilder, private Auth: AuthService, private router: Router)
  {
    this.form = this.fb.group({
      email: ['', {validators: [Validators.required, Validators.pattern(this.emailPattern)], updateOn: 'blur'}],
      password: ['', Validators.required]
    });
  }

  login(): void
  {
    const val = this.form.value;
    if(val.email && val.password)
    {
      this.Auth.loginUser(val.email, val.password).subscribe(
        res => {
          document.getElementById("error").style.display = "none";
          if(res.loginState == "LOGIN_STATE_FAILED")
          {
            document.getElementById("error").style.display = "block";
          }
          else
          {
            sessionStorage.setItem('token', res.token);
            sessionStorage.setItem('uid', jwt_decode(res.token).id);
            this.router.navigateByUrl("/home")
          }
        },
        err =>
        {
          console.log(err.error);
          // document.getElementById("error").innerHTML = "Login error" + err.error.userId;
          // document.getElementById("error").style.display = "block";
        }
      )
    }
  }

  get email()
  {
    return this.form.get('email');
  }

  ngOnInit() {
    if(this.Auth.isLoggedIn())
    {
      this.router.navigateByUrl("/home");
    }
  }

}
