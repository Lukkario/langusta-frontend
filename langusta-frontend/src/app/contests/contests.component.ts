import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-contests',
  templateUrl: './contests.component.html',
  styleUrls: ['./contests.component.css', '../bootstrap.css']
})
export class ContestsComponent implements OnInit {

  constructor(private http: HttpClient, private fb:FormBuilder) {
    this.form = this.fb.group({
      title: ['', {validators: [Validators.required], updateOn: 'blur'}],
      platform: ['', {validators: [Validators.required], updateOn: 'blur'}],
      postLink: ['',{validators: [Validators.required], updateOn: 'blur'}],
      endTime:['', {validators: [Validators.required], updateOn: 'blur'}]
    })
   }

  ngOnInit() {
    this.getPages();
  }

  form:FormGroup;
  private fbPages = [];
  private fbPosts = [];
  private isFacebook = false;
  private isYoutube = false;
  private time;


  private getPages(): void
  {
    this.http.get<any>('/api/facebook/page/all').subscribe(
      res =>
      {
        if(res.state === "STATUS_SUCCESS")
        {
          this.fbPages = res.payload.accounts.data;
        }
        else
        {
          document.getElementById("info2").style.display = 'block';
        }
      },
      err =>
      {
        document.getElementById("info").style.display = 'none';
        document.getElementById("error").style.display = 'block';
        document.getElementById("error").innerHTML = "<strong>Sending message error!</strong> " + err.error.additionalInfo;
      }
    )
  }

  private getPosts(pageID: any): void
  {
    console.log(pageID);
    this.http.get<any>('api/facebook/page/' + pageID + '/posts').subscribe(
      res =>
      {
        this.fbPosts = res.payload.data;
        // console.log(this.fbPosts);
      },
      err =>
      {
        console.log("getPosts error");
        console.log(err);
      }
    )
  }

  private send(): void
  {
    const val = this.form.value;
    console.log(val);
    this.time = Date.parse(val.endTime);
    val.endTime = this.time/1000;
    console.log(val);

    document.getElementById("success").style.display = 'none';
    document.getElementById("error").style.display = 'none';

    if(val.endTime !== NaN && val.title && val.platform)
    {
      document.getElementById("info").style.display = 'block';
      const title = val.title;
      const platform = val.platform;
      const postLink = val.postLink;
      const endTime = val.endTime;
      this.http.post<any>('/api/contest', {
        title, platform, postLink, endTime
      }).subscribe(
        res =>
        {
          console.log(res);
          document.getElementById("info").style.display = 'none';
          if(res.state === "STATUS_SUCCESS")
          {
            document.getElementById("success").style.display = 'block';
          }
          else
          {
            document.getElementById("error").style.display = 'block';
            document.getElementById("error").innerHTML = "<strong>Sending message error!</strong> " + res.additionalInfo;
          }
        },
        err =>
        {
          console.log(err);
          document.getElementById("info").style.display = 'none';
          document.getElementById("error").style.display = 'block';
          document.getElementById("error").innerHTML = "<strong>Sending message error!</strong> " + err.error.additionalInfo;
        }
      )
    }
    else
    {
      document.getElementById("warning").style.display = 'block';
    }
  }

private radioChange(event: number): void
{
  if(event == 1)
  {
    this.isFacebook = false;
    this.isYoutube = true;
  }
  else
  {
    this.isYoutube = false;
    this.isFacebook = true;
  }
}

}
