import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '../../../node_modules/@angular/router';
import { HomeComponent } from '../home/home.component';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css', '../bootstrap.css', '../simple-line-icons.css']
})
export class TopNavComponent implements OnInit {

  constructor(private Auth: AuthService, private router: Router, private home: HomeComponent, private http: HttpClient) { }

  ngOnInit() {
    this.getUserFullName();
  }

  userFullName = "";

  logout(): void
  {
    this.Auth.logout();
    this.router.navigateByUrl("/login");
  }

  private getUserFullName(): void
  {
    this.http.get<any>('/api/user/me').subscribe(
      res =>
      {
        this.userFullName = res.payload.firstName + " " + res.payload.lastName;
      }
    );
  }

}
