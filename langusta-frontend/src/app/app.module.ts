import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { RightNavComponent } from './right-nav/right-nav.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthInterceptorService } from './auth-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { LandpadComponent } from './landpad/landpad.component';
import { AddFbPostComponent } from './add-fb-post/add-fb-post.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { SendMessageFbComponent } from './send-message-fb/send-message-fb.component';
import { ContestsComponent } from './contests/contests.component';
import { SettingsComponent } from './settings/settings.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    TopNavComponent,
    RightNavComponent,
    DashboardComponent,
    RegisterComponent,
    LandpadComponent,
    AddFbPostComponent,
    WelcomeComponent,
    SendMessageFbComponent,
    ContestsComponent,
    SettingsComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      [
        {
          path: '',
          component: WelcomeComponent
        },
        {
          path: 'login',
          component: LoginComponent
        },
        {
          path: 'register',
          component: RegisterComponent
        },
        {
          path: 'home',
          component: HomeComponent
        },
        {
          path: '**',
          component: WelcomeComponent
        }
      ]
    )
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  exports:
  [
    FormsModule,
    ReactiveFormsModule
  ],
  bootstrap: [AppComponent],
  entryComponents: [DashboardComponent, LandpadComponent, AddFbPostComponent, SendMessageFbComponent, ContestsComponent, SettingsComponent]
})
export class AppModule { }
