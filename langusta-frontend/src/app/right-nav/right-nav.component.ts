import { Component, OnInit } from '@angular/core';
import { HomeComponent } from '../home/home.component';

@Component({
  selector: 'app-right-nav',
  templateUrl: './right-nav.component.html',
  styleUrls: ['./right-nav.component.css', '../bootstrap.css', '../simple-line-icons.css']
})
export class RightNavComponent implements OnInit {

  constructor(private home: HomeComponent) { }

  ngOnInit() {
  }

  loadDashboard()
  {
    this.home.viewDashboardComponent();
  }

  loadLandpad()
  {
    this.home.viewLandpadComponent();
  }

  addPost()
  {
    this.home.viewAddFbPostComponent();
  }

  sendMessage()
  {
    this.home.viewSendMessageFb();
  }

  createContest()
  {
    this.home.viewMakeContest();
  }

  showSettings()
  {
    this.home.viewSettings();
  }

}
