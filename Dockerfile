FROM nginx
COPY langusta-frontend/dist/langusta-frontend /usr/share/nginx/html
COPY docker-nginx.conf /etc/nginx/nginx.conf
EXPOSE 80